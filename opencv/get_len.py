# -*- coding: utf-8 -*-
import cv2
import numpy as np
import sys
import time
import os
import glob
import sys
import re
from PIL import Image
import rospy
import cv2
import sys
import os
import shutil
from sensor_msgs.msg import Image
from std_msgs.msg import String
from cv_bridge import CvBridge, CvBridgeError

"""
wsize=480
hsize=480
files = glob.glob('./crowd/*.jpg')
i=0
#ファイル一覧をループ
for f in files:
    img = Image.open(f)
    img_resize = img.resize((wsize, hsize))

    newfname = str(i)+".jpeg"
    print(newfname)
    img_resize.save("./crowd/"+newfname)
    i+=1
"""
    
body_cascade_file = "/home/matsudayamato/ros_ws/src/spr/requesting_an_operator_pkg/etc/opencv-3.3.1/data/haarcascades/haarcascade_fullbody.xml"
body_cascade = cv2.CascadeClassifier(body_cascade_file)


class Face_cut():
	def __init__(self):
		self.sub = rospy.Subscriber("/camera/rgb/image_raw", Image, self.get_image)
		self.node_end = rospy.Publisher("messenger", String, queue_size=10, latch=True)
		self.bridge = CvBridge()
		self.image_org = None
	def get_image(self, img):
		self.image_org = self.bridge.imgmsg_to_cv2(img, "bgr8")
	def main(self, msg):
		print(msg)
		if self.image_org==None:
			return
		gray = cv2.cvtColor(self.image_org, cv2.COLOR_BGR2GRAY)
		body = body_cascade.detectMultiScale(gray, 1.5, 3)
		for i in range(len(body)):
			[x,y,w,h]=body[i]
			imgCroped=cv2.resize(self.image_org[y:y+h,x:x+w],(480,480))
		if len(body) == 1:### if len(body)>=3:
			time.sleep(1)
			cv2.imwrite("./crowd/crowd"+str(cnt)+".jpg",img)		
			cnt+=1
			print "take{}".format(cnt)
		cv2.imshow('img', self.image_org)
		cv2.waitKey()
		print len(body)
def callback(data):
    if data.data== '02':
		get_image()

if __name__ == "__main__":
	rospy.init_node('req_operator_spr')
	fd = Face_cut()
	fsub = fd.sub
	rate = rospy.Rate(1)
	while not rospy.is_shutdown():
		fd.main("ok")
		rate.sleep()
		rospy.spin()
    #sub02 = rospy.Subscriber('face_recognize', String, callback)
    #sub0x=rospy.Subscriber('/camera/rgb/image_raw',Image , get_images)
    #rospy.spin()
