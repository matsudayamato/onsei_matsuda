import cv2
import numpy as np
import sys
#Web
cap = cv2.VideoCapture(0)

#
face_cascade_file = "/home/matsudayamato/ros_ws/src/spr/requesting_an_operator_pkg/etc/opencv-3.3.1/data/haarcascades/haarcascade_frontalcatface.xml"
face_cascade = cv2.CascadeClassifier(face_cascade_file)

#
def mosaic(img, rect, size):
    (x1, y1, x2, y2) = rect
    w = x2 - x1
    h = y2 - y1
    i_rect = img[y1:y2, x1:x2]

    i_small = cv2.resize(i_rect, (size, size))
    i_mos = cv2.resize(i_small, (w, h), interpolation=cv2.INTER_AREA)

    img2 = img.copy()
    img2[y1:y2, x1:x2] = i_mos
    return img2

cnt=0
while True:
    #
    ret, img = cap.read()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.5, 3)


    if len(faces) != 0:
        #for (x, y, w, h) in faces:
            #img = mosaic(img, (x, y, x+w, y+h), 10)
        #    cv2.imwrite("./apr.jpg",img)
        #    sys.exit()
        cnt+=1    
    cv2.imshow('img', img)
    #ESCorEnter
    k = cv2.waitKey(1)
    if k == 13:
        break
    if cnt==2:
        break
print len(faces)
cap.release()
cv2.destroyAllWindows()
