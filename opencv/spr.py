import sys
#sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
import cv2
import numpy as np

upperbody_cascade_path = '/home/matsudayamato/ros_ws/src/spr/requesting_an_operator_pkg/etc/opencv-3.3.1/data/haarcascades/haarcascade_upperbody.xml'
eye_cascade_path = '/home/matsudayamato/ros_ws/src/spr/requesting_an_operator_pkg/etc/opencv-3.3.1/data/haarcascades/haarcascade_eye.xml'

upperbody_cascade = cv2.CascadeClassifier(upperbody_cascade_path)
eye_cascade = cv2.CascadeClassifier(eye_cascade_path)

ORG_WINDOW_NAME = "cap"
GAUSSIAN_WINDOW_NAME = "cap_gray"

cap = cv2.VideoCapture(0)  
end_flag, c_frame = cap.read()
height, width, channels = c_frame.shape

cv2.namedWindow(ORG_WINDOW_NAME)
cv2.namedWindow(GAUSSIAN_WINDOW_NAME)


while end_flag == True:

    img = c_frame
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    body = upperbody_cascade.detectMultiScale(img_gray, minSize=(100,100))

    for x, y, w, h in body:
        cv2.rectangle(img_gray, (x, y), (x + w, y + h), (255, 0, 0), 2)
        body = img_gray[y: y + h, x: x + w]
        body_gray = img_gray[y: y + h, x: x + w]
        eyes = eye_cascade.detectMultiScale(body_gray)
        for (ex, ey, ew, eh) in eyes:
            cv2.rectangle(body, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)

    cv2.imshow(ORG_WINDOW_NAME, c_frame)
    cv2.imshow(GAUSSIAN_WINDOW_NAME, img_gray)

    k = cv2.waitKey(1)
    if k == 27:
        break

    end_flag, c_frame = cap.read()
cv2.waitKey(0)
cv2.destroyAllWindows()

