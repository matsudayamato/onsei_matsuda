#import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
xmin = 0.0
xmax = 0.01
x = np.arange(xmin,xmax,0.0001)
exp =np.exp(-5000*x/17)/40
cos =np.cos(((10**4)*13*x)/34)
sin =(1/13)*(np.sin(((10**4)*x*13)/34))
y = exp*(cos+sin)
plt.plot(x,y)
#plt.xlim(xmin,xmax)
plt.ylim(-0.03,0.03)
plt.show()
