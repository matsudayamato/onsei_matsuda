#!/bin/sh
echo "--------caution---------"
echo "do you connect re-speaker?(y/n)"
read ret
if [ $ret = 'y' ]; then
    echo "install pyusb click?(y/n)"
    read pyusb
    echo "install usb_4_mic_array?(y/n)"
    read usb_4_mic_array
    if [ $pyusb = 'y' ]; then
        sudo apt-get update
        sudo pip install pyusb click
    fi
    if [ $usb_4_mic_array = 'y' ]; then
        cd
        git clone https://github.com/respeaker/usb_4_mic_array.git
        cd usb_4_mic_array
        sudo python dfu.py --download 6_channels_firmware.bin
    fi
fi
echo end
