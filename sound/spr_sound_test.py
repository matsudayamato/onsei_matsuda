#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
from std_msgs.msg import String
import socket #juliusサーバにアクセスするために必要なモジュール
import re
from time import sleep
from janome.tokenizer import Tokenizer
qa_dict = {} 
import math

rospy.init_node('talker_julius')
pub = rospy.Publisher('chatter',String,queue_size=10)
rate = rospy.Rate(10)
julius_str = String()
HOST = "localhost" #アドレス　自分のパソコンでjuliusサーバを立てる場合はlocalhostまたは127.0.0.1を指定する
PORT = 10500 #ポート

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect((HOST, PORT))

# サーバの応答を受信

while 1:
    try:
	data = ""
        while 1:
            response = client.recv(1024) #音声認識結果を文字列として受け取る(xml形式)
	    #print response
	    #sleep(1)
            if '<RECOGOUT>\n' in response:
		data = response
	    elif 'WHYPO WORD=' in response:
		data = data + response
	    else:
		data = ""

	    #print data
          
            if '</RECOGOUT>' in data:
                text = ""
               	line_list = data.split('\n')
		#print line_list
               	for line in line_list:
                    if 'WHYPO' in line:
                    	word = re.compile('WORD="((?!").)+"').search(line)
			#print word
                       	if word:
                            text = text + word.group().split('"')[1]
			    #print "a"
                
              	#if text is not "":
                    #print text
                print text        
               	#data = ""


        	text = text.decode('utf-8')

                with open('../data/q_list.txt', 'r') as f: 
                    qa_list = f.readlines()
                    for qa in qa_list:
                        qa = qa.rstrip().decode('utf-8').split(',') 
                        qa_dict[qa[0]] = qa[1] 

                t = Tokenizer(text) 
                list1 = []
                for token in t.tokenize(text): 
	            #print token
	            if u'名詞' in token.part_of_speech:
  	                list1.append(token.surface)
	            elif u'動詞' in token.part_of_speech:
                        list1.append(token.surface)

                def calc_cos(A,B):
	                lengthA = math.sqrt(len(A))
	                #print len(A)
	                lengthB = math.sqrt(len(B))
	                #print len(B)
	                match = 0.0
	                for a in A:
		                if a in B:
			                match += 1
			                #print match
			if (lengthA != 0 and lengthB != 0):
	                	cos = match/(lengthB*lengthA)
	                return cos

                i = 0
                max = 0
                for qa in qa_dict.keys():
	                listi = []
	                for token in t.tokenize(qa_dict.keys()[i]):
		                if u'名詞' in token.part_of_speech:
			                 listi.append(token.surface)
		                elif u'動詞' in token.part_of_speech:
  			                 listi.append(token.surface)	
	                cosAB = calc_cos(listi,list1)
	                #print cosAB
	                #print listi
	                if cosAB > max:
		                max = cosAB
		                text = qa_dict.keys()[i]
	                i += 1
                if max > 0.25:
	                print max
	                text = qa_dict[text].encode('utf-8')
                else:
			text  = '分かりません分かりません分かりません。'
	                

		julius_str.data = text
		pub.publish(julius_str)
		rate.sleep()
		
		data = ""
	
    except KeyboardInterrupt:
        #例外が発生した場合はクライアントを一旦消し，3秒後にクライアントを再作成・再接続する
        client.close()
        sleep(3)
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect((HOST, PORT))
