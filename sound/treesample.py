import treetaggerwrapper as ttw

def main():
    tagger = ttw.TreeTagger(TAGLANG='en',TAGPARFILE="/home/matsudayamato/treetagger/lib/english.par")
    tags = tagger.TagText(u'A quick brown fox jumps over the lazy black dog.')
    print(tags)

if __name__ == '__main__':
    main()
