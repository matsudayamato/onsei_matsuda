from pocketsphinx import LiveSpeech
import sys
text1 ='forward'
text2 = 'start'
phrase = 'for'
speech = LiveSpeech(lm=False, keyphrase='start  game', kws_threshold=1e-20)
while phrase != text1:
    for phrase in speech:
        print(phrase)
        phrase ='forward'
        break

speech = LiveSpeech(lm=False, keyphrase='there is ', kws_threshold=1e-20)
while phrase != text2:
    for phrase in speech:
        print(phrase)
        phrase ='start'
        break
