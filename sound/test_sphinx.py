from pocketsphinx import LiveSpeech
import rospy
from std_msgs.msg import String
def recogn():
    print "recogn"
    speech = LiveSpeech(lm=False, keyphrase='forward', kws_threshold=1e-40)
    for phrase in speech:
        print(phrase.segments(detailed=True))

if __name__ == "__main__":
    rospy.init_node("test_sphinx")
    msg = rospy.wait_for_message("/r",String)
    print msg
    msg2 = rospy.wait_for_message("/s",String)
    print msg2
    if msg2=="02":
        recogn()

