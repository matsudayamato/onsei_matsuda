import matplotlib.pyplot as plt
import numpy
# read file
with open('/home/matsudayamato/onsei_matsuda/alpha/eyeopen1_20181201.2.txt') as f:
    l =f.read().splitlines()
l2= [float(x) for x in l]
sum  = 0
zerocross = []

#make average Volt
for i in range(0,15000):
    sum = sum + l2[i]
#print 'sum = '+ str(sum)
avh = sum/15000.0
print 'average Volt = '+str(avh)

#make average Hz
for i in range(0,14999):
    if((l2[i]-avh)*(l2[i+1]-avh)<0):
        x = avh/(500*(l2[i+1]-l2[i]))+i/500+l2[i]/(l2[i+1]+l2[i])
        zerocross.append(x)
zlen = len(zerocross)
zalpha =[]
for i in range(zlen-1):
    if(zerocross[i+1]-zerocross[i]>1.0/13 and zerocross[i+1]-zerocross[i]<0.125):
        zalpha.append(1/(zerocross[i+1]-zerocross[i]))
alsum = 0
for i in zalpha:
    alsum = alsum + i
print 'average Hz = '+str(alsum/len(zalpha))
