import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
xmin = -5
xmax = 5
a = 0
x = np.arange(xmin, xmax, 0.1)
y_sin = np.sin(x)
y_cos = a*x
plt.plot(x, y_sin)
plt.plot(x, y_cos)
plt.hlines([-1, 1], xmin, xmax, linestyles="dashed")
plt.title(r"$\sin(x)$")
plt.xlim(xmin, xmax)
plt.ylim(-1.3, 1.3)
#plt.show()
plt.savefig('figure.png')
