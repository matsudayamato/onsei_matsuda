#! /bin/bash
sudo apt update
sudo apt upgrade
sudo apt-get install -y wget
sudo apt install git
sudo apt-get install udev
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
sudo apt update
sudo apt install ros-melodic-desktop-full
sudo rosdep init
rosdep update
echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc
source ~/.bashrc
sudo apt install python-rosinstall python-rosinstall-generator python-wstool build-essential

cd
wget https://github.com/ros-drivers/libfreenect/archive/ros-devel.zip
unzip ros-devel.zip
cd libfreenect-ros-devel
mkdir build
cd build
cmake -L ..
make
sudo make install
cd /etc/udev/rules.d/
sudo touch 51-kinect.rules
echo 'SUBSYSTEM=="usb", ATTR{idVendor}=="045e", ATTR{idProduct}=="02b0", MODE="0666"' | sudo tee -a 51-kinect.rules
echo 'SUBSYSTEM=="usb", ATTR{idVendor}=="045e", ATTR{idProduct}=="02ad", MODE="0666"' | sudo tee -a 51-kinect.rules
echo 'SUBSYSTEM=="usb", ATTR{idVendor}=="045e", ATTR{idProduct}=="02ae", MODE="0666"' | sudo tee -a 51-kinect.rules
echo 'SUBSYSTEM=="usb", ATTR{idVendor}=="045e", ATTR{idProduct}=="02c2", MODE="0666"' | sudo tee -a 51-kinect.rules
echo 'SUBSYSTEM=="usb", ATTR{idVendor}=="045e", ATTR{idProduct}=="02be", MODE="0666"' | sudo tee -a 51-kinect.rules
echo 'SUBSYSTEM=="usb", ATTR{idVendor}=="045e", ATTR{idProduct}=="02bf", MODE="0666"' | sudo tee -a 51-kinect.rules
sudo udevadm trigger

mkdir -p ros_ws/src
cd ros_ws/
curl -sLf https://raw.githubusercontent.com/gaunthan/Turtlebot2-On-Melodic/master/install_basic.sh | bash
git clone https://github.com/ros-drivers/rgbd_launch
git clone https://github.com/ros-drivers/freenect_stack.git
cd ..
catkin_make

echo 'source ~/ros_ws/devel/setup.bash' | >> ~./bashrc
echo "source ~/.bashrc" >> ~/.bashrc

